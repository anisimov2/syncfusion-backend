function DataWithIterator(data) {
  const keys = Object.keys(data[0]).filter(key => key !== 'subtasks');
  const recursiveIterator  = function* (data) {
    if (!data) { return; }

    for (const el of data) {
      const val = el;
      yield val;

      if (val.subtasks?.length) {
        yield *recursiveIterator(val.subtasks);
      }
    }
  };

  const clone = (data) => {
    return data.concat().map((el) => {
      const c = Object.assign({}, el);
      if (c.subtasks) {
        c.subtasks = clone(c.subtasks);
      }

      keys.forEach(key => {
        if (typeof c[key] === 'object') {
          c[key] = Object.assign(c[key], {});
        }
      });

      return c;
    });
  };

  const filter = (data, callback) => {
    return data.concat().map(el => {
      if (el.subtasks) {
        el.subtasks = filter(el.subtasks, callback);
      }
      return el;
    }).filter(callback);;
  }

  return {
    data: clone(data),
    forEach: function (callback) {
      for (let el of this) {
        callback(el);
      }
      return this.data;
    },
    filter: function (callback) {
      return filter(this.data, callback);
    },
    [Symbol.iterator]: function* () {
      yield *recursiveIterator(this.data);
      return;
    }
  }
}

module.exports = {
  DataWithIterator
}
