const { dataGenerator } = require('./data-generator');
const { DataWithIterator } = require('./data-with-iterator');

class DataService {
  #data = dataGenerator.generateData();

  get data() {
    return this.#data;
  }

  renameColumn(name, newName) {
    const dataWithIterator = DataWithIterator(this.#data);
    for (let el of dataWithIterator) {
      el[newName] = el[name];
      if (el[name]) delete el[name];
    }
    this.#data = dataWithIterator.data;
    return this.#data;
  }

  editColumnValue(name, newValue) {
    const dataWithIterator = DataWithIterator(this.#data);
    for (const data of dataWithIterator) {
      data[name] = newValue;
    }
    this.#data = dataWithIterator.data;
    return this.#data;
  }

  #getElement = (complexIndex) => {
    const complexIndexCopy = [...complexIndex];
    const rootIndex = complexIndexCopy.shift();
    let value = this.#data[rootIndex];
    complexIndexCopy.forEach((index) => {
      value = value?.subtasks[index];
      if (!value) throw new Error('Index is incorrect');
    });
    return value;
  }

  #getParentElement = (complexIndex) => {
    const complexIndexCopy = [...complexIndex];
    const rootIndex = complexIndexCopy.shift();
    if (!complexIndexCopy.length) return this.#data;
    complexIndexCopy.pop();
    let value = this.#data[rootIndex];
    complexIndexCopy.forEach((index) => {
      value = value?.subtasks[index];
      if (!value) throw new Error('Index is incorrect');
    });
    return value;
  }

  editRow(complexIndex, newValue) {
    const value = this.#getElement(complexIndex);
    Object.keys(newValue).forEach((key) => {
      value[key] = newValue[key];
    });
  }

  addAsChild(targetIndex, newValue) {
    const value = this.#getElement(targetIndex);
    if (Array.isArray(value?.subtasks)) {
      value?.subtasks.push(newValue);
    } else {
      value.subtasks = [newValue];
    }
  }

  addAsSubling(targetIndex, data, isNext) {
    const elIndex = targetIndex[targetIndex.length - 1];
    const parent = (targetIndex.length === 1) ? this.#data : this.#getElement(targetIndex)?.subtasks;
    const newElIndex = elIndex + (isNext ? elIndex + 1 : elIndex - 1);
    parent.splice(newElIndex === -1 ? 0 : newElIndex, 0, data);
  }

  deleteRows(complexIndexes) {
    const deletedElements = complexIndexes.map(this.#getElement);
    const parents = complexIndexes.map(this.#getParentElement);
    deletedElements.forEach((row, i) => {
      const filterCallback = e => e !== row;
      if(parents[i]?.subtasks) {
        parents[i].subtasks = parents[i].subtasks.filter(filterCallback);
      } else {
        this.#data = this.#data.filter(filterCallback);
      }
    });
  }

  #moveNode(el, to) {
    const newParent = this.#getParentElement(to);
    const newElInd = to.pop();
    if (typeof newParent === 'object' && !Array.isArray(newParent) && !newParent?.subtasks) {
      newParent.subtasks = [];
    }
    const newParentArr = newParent?.subtasks || newParent;
    newParentArr.splice(newElInd, 0, el);
  }


  #cut(copiedRows, parents) {
    copiedRows.forEach((row, i) => {
      const parent = parents[i];
      if (parent?.subtasks) {
        if (!copiedRows.includes(parent)) {
          parent.subtasks = parent.subtasks.filter(el => el !== row);
        }
      } else {
        this.#data = this.#data.filter(el => el !== row);
      }
    });
  }

  moveNodes(fromArr, toArr) {
    const copiedRows = fromArr.map(this.#getElement);
    this.deleteRows(fromArr);
    copiedRows.forEach((row, ind) => {
      this.#moveNode(row, toArr[ind]);
    });
  }

  pasteNodes(fromArr, to, isCut, asChild) {
    const target = this.#getElement(to);
    const parents = fromArr.map(this.#getParentElement);
    const copiedRows = fromArr.map(this.#getElement);
    to.pop();
    const parentTarget = this.#getParentElement(to);
    if (asChild) {
      target.subtasks = target.subtasks ? target.subtasks.concat(copiedRows) : copiedRows;
    } else {
      let parentArr = parentTarget?.subtasks || parentTarget;
      const targetIndex = parentArr?.indexOf(target);
      if (targetIndex !== undefined && targetIndex !== -1 && parentArr) {
        parentArr.splice(targetIndex, 0, ...copiedRows);
      }
    }
    if (isCut) {
      this.#cut(copiedRows, parents);
    }
  }
}

const dataService = new DataService();

module.exports = {
  dataService
};
